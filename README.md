# Shadowrun: Electrochoc

A french cyberpunk novel written by Frédéric Potvin.

You can download the novel in PDF, MOBI or EPUB from this repository.

![Front cover](cover-front.png)

![Back cover](cover-back.png)

