Licensing for this project is very simple:

1. All references to the Shadowrun universe are copyrighted to the current Shadowrun trademark owner (it change quite often).

2. Characters and stories created by me are under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
